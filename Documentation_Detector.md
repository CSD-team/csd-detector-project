Module: Detector
============================
This module calculates the possible volumes for a particle flying throw a detector with a few layers.<br>

To use this module, its enough to create one instance of CDetector and use the function CDetector.detect (particle).

Classes:
============================

CDetector<br>
CLayer



CDetector:
============================

Functions:

----------------------------

\_\_init\_\_(LayerArray = [100,110,120,130,140], Magnet=0.522)<br>

detect (vector)

**\_\_init\_\_(LayerArray = [100,110,120,130,140], Magnet=0.522):**<br>
Creates a few instances of CLayer with standartvalues {100, 110, 120, 130, 140} which represents the (z-)position of the Layers.<br>
The default B-Field is at 0.522x10^-6 Tesla, enter here microtesla.<br>

*Arguments:*<br>
LayerArray - Array with z-position in cm of Layers - default: 100,110,120,130,140
Magnet - Magnet Field along the x-axis, insert Mikrotesla - default: 0.522 muT

**detect (particle):**<br>
Loop over all Layers the function CLayer.detect (particle).<br><br>
*Arguments:*<br>
class particle  it needs the public membervalues:<br>
vx, vy, vz  velocity in 3 dimensions<br>
q  charge in natural units<br>
m  in MeV<br>

*Return:*<br>
Returns a numpy array with Layernumber+1 entries, each Tuple includes 4 Tuples with a upper and a lower bound for the room coordinates and the time.<br>
The first tuple contains the source point (so the (0,0,0,0) point).<br>
This means:<br>
e.g. returnvalue [a][b][c]<br>
a  [0,5] select Layer<br>
b  0 is x, 1 is y, 2 is z, 3 the time<br>
c  0 is the upper bound, 1 the lower bound<br><br>

If one Layer is not hit, returnvalue is None.<br>





CLayer:
============================

Functions:

----------------------------


\_\_init\_\_(Position, BField)<br>

detect (particle)



**\_\_init\_\_(Position, BField):**<br>

Just copy the argument.<br><br>
*Arguments:*<br>
Position: Location of the Layer at the z-axis<br>
BField: Strength of B-field in mikrotesla

**detect (particle):**<br>
Calculate the bounds of hit grid.<br><br>
*Arguments:*<br>
class particle  it needs the public membervalues:<br>
vx, vy, vz  velocity in 3 dimensions<br>
q  charge in natural units<br>
m  in MeV<br>

*Return:*<br>
Returns 4 Tuples with a higher and a lower angle bound for cartesien coordinates and the time.<br>
This means:<br>
e.g. returnvalue [a][b]<br>
a  0 is x, 1 is y, 2 is z, 3 the time<br>
b  0 is the upper bound, 1 the lower bound<br><br>
If this Layer is not hit, the Tuple is ((None, None),( None, None),(self.Postion,self.Position),(t,t)<br>


