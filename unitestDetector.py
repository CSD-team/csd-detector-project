import unittest
import Detector

class Particle:
    def __init__(self,vx,vy,vz,m,q):
        self.vx = vx
        self.vy = vy
        self.vz = vz
        self.m  = m
        self.q  = q
 
class TestDetector(unittest.TestCase):
    
    def compare (self, A,B, delta=None):
        for a in range(len(A)):
            for b in range(len(A[a])):
                for c in range (len(A[a][b])):
                    #print (a,b,c)
                    self.assertAlmostEqual(A[a][b][c],B[a][b][c], delta = delta)
 
    #numbers describes vx_vy_vz_m_q
    def test_0_0_500_e_neg(self):
        Det = Detector.Detector ()
        Par = Particle(0,0,500,0.522,-1)
        Exact = [[[0,0],[0,0],[0,0],[0,0]],
                [[0,0],[-10.1020,-10.1020],[100,100],[0.2013,0.2013]],
                [[0,0],[-12.2500,-12.2500],[110,110],[0.2218,0.2218]],
                [[0,0],[-14.6135,-14.6135],[120,120],[0.2423,0.2423]],
                [[0,0],[-17.1956,-17.1956],[130,130],[0.2630,0.2630]],
                [[0,0],[-20,-20],[140,140],[0.2837,0.2837]]]
        Calc = Det.detect(Par)
        self.compare(Calc,Exact, 0.0026)
        
    def test_0_neg50_500_e_pos(self):
        Det = Detector.Detector ()
        Par = Particle(0,-50,500,0.522,1)
        Exact = [[[0,0],[0,0],[0,0],[0,0]],
                [[0,0],[0.0000,0.0000],[100,100],[0.1993,0.1993]],
                [[0,0],[1.1012,1.1012],[110,110],[0.2193,0.2193]],
                [[0,0],[2.4057,2.4057],[120,120],[0.2394,0.2394]],
                [[0,0],[3.9153,3.9153],[130,130],[0.2595,0.2595]],
                [[0,0],[5.6317,5.6317],[140,140],[0.2797,0.2797]]]
        Calc = Det.detect(Par)
        self.compare(Calc,Exact, 0.0026)
        
    def test_158_0_450_e_pos(self):
        Det = Detector.Detector ()
        Par = Particle(158,0,450,0.522,1)
        Exact = [[[0,0],[0,0],[0,0],[0,0]],
                [[35.4067,35.4067],[11.2517,11.2517],[100,100],[0.2240,0.2240]],
                [[39.0175,39.0175],[13.6515,13.6515],[110,110],[0.2469,0.2469]],
                [[42.6493,42.6493],[16.2950,16.2950],[120,120],[0.2699,0.2699]],
                [[46.3044,46.3044],[19.1868,19.1868],[130,130],[0.2930,0.2930]],
                [[49.9851,49.9851],[22.3319,22.3319],[140,140],[0.3163,0.3163]]]
        Calc = Det.detect(Par)
        self.compare(Calc,Exact, 0.0026)
                    
	
if __name__ == '__main__':
    unittest.main()