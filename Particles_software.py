# -*- coding: utf-8 -*-
import numpy as np

  
    
class Particle:
    """Create angles, mass and velocity to particles on a plane"""
    
    def __init__(self):       
        """The angles are created randomly onto a projectet square"""
        self.theta = np.random.uniform(-np.arctan(0.5), np.arctan(0.5))
        self.phi = np.random.uniform(-np.arctan(0.5), np.arctan(0.5))
       
        
        """If we want two different masses (electron, proton), I have here 
        created them with equal probability"""
        self.e = np.random.uniform(0,1)
        if self.e >= 0.5:
            self.m = 0.511 #MeV
            self.q = -1
        else:
            self.m = 0.511 #Mev
            self.q = 1
        
        
        """Velocity: random velocity between X and Y"""
        self.v = np.random.uniform(0.2, 0.6) #Natural units
        #Alter the range of velocities
        #Perhaps make vx, vy, vz
        self.vx = np.random.uniform(0, 80)
        self.vy = np.random.uniform(0, 80)
        self.vz = np.random.uniform(200, 500)

        
        """Charge, 1,-1"""
        self.k = np.random.uniform(1, 0)
        if self.k > 0.5:
            self.q = 1
        else:
            self.q = -1
