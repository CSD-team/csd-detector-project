import unittest
from analysis import Analysis
from Detector import Detector

"""
Test interplay of detector and analysis
"""

class Particle():
    def __init__(self):
        self.vx = 30
        self.vy = 20
        self.vz = 300
        self.m = 0.511
        self.q = -1

class Test(unittest.TestCase):

    def test_simulation(self):
        PART = Particle()
        DET = Detector()
        DETRes = DET.detect(PART)
        ANA = Analysis()
        ANA.fill(DETRes)
        ANA.Fit()

        ANA.output()

        self.assertTrue(PART.vx < ANA.results['vx'][0] + 3*ANA.results['vx'][1])
        self.assertTrue(PART.vx > ANA.results['vx'][0] - 3*ANA.results['vx'][1])
        self.assertTrue(PART.vy < ANA.results['vy'][0] + 3*ANA.results['vy'][1])
        self.assertTrue(PART.vy > ANA.results['vy'][0] - 3*ANA.results['vy'][1])
        self.assertTrue(PART.vz < ANA.results['vz'][0] + 3*ANA.results['vz'][1])
        self.assertTrue(PART.vz > ANA.results['vz'][0] - 3*ANA.results['vz'][1])

