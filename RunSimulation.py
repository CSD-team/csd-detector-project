"""
Script to run the full detector simulation.
"""

""" Import all necessary classes. """
from Particles_software import Particle
from Detector import Layer, Detector
from analysis import Analysis

""" Create a Particle """
PART = Particle()

""" Hand the particle to detector class to detect particle and measure its
properties. """
DET = Detector()
DETRes = DET.detect(PART)


""" Hand detector results to Analysis class and compute results. If the particle
did not hit all 5 layers, an error message occures. The output is stored in 
a results.csv file, the plot is saved in a 3D.png file. When simulation isrun
again, results.csv and 3D.png are overwritten. """
try:
    ANA = Analysis()
    ANA.fill(DETRes)
    ANA.Fit()
    ANA.save()
    ANA.plot()

    ANA.output()
    
except TypeError:
    print("Particle did not hit all 5 layers. Run new Simulation.")